#!/usr/bin/env bash
###############################################################################
##                                                                           ##
## Purpose of script: Merge multiple InterProScan XML files                  ##
##                                                                           ##
###############################################################################

# var settings 
args=("$@")
MERGED_XML=${args[0]}
QUERY_TYPE=${args[1]}

# Collect hearder and tail of the XML
head -n1 -q *.xml | uniq > header_XML
tail -n1 -q *.xml | uniq > tail_XML

# Format individual InterProScan XML
sed -i '1d' *.xml
sed -i '$d' *.xml

# Merge individual InterProScan XML
cat *.xml > merged.xml

# Reformat merged InterProScan XML
cat header_XML merged.xml tail_XML > $MERGED_XML
