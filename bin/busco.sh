#!/usr/bin/env bash
###############################################################################
##                                                                           ##
## Purpose of script: Run BUSCO to evaluate transcriptome completness        ##
##                                                                           ##
###############################################################################

# var settings 
args=("$@")
NCPUS=${args[0]}
TRANSCRIPTOME=${args[1]}
OUTPUT=${args[2]}
LINEAGE=${args[3]}
QTYPE=${args[4]}
DB=${args[5]}
LOGCMD=${args[6]}

if $QTYPE == 'n';
then
        ANALYSE="transcriptome"
else
        ANALYSE="proteome"
fi

if [[ $(echo $LINEAGE | grep -w ^auto\-lineage$) ]];
then
        CMD="busco -i $TRANSCRIPTOME -o ${OUTPUT}_auto --auto-lineage -m $ANALYSE -c $NCPUS --download_path $DB --offline"
elif [[ $(echo $LINEAGE | grep -w ^auto\-lineage\-prok$) ]];
then
        CMD="busco -i $TRANSCRIPTOME -o ${OUTPUT}_auto-prok --auto-lineage-prok -m $ANALYSE -c $NCPUS --download_path $DB --offline"
elif [[ $(echo $LINEAGE | grep -w ^auto\-lineage\-euk$) ]];
then
        CMD="busco -i $TRANSCRIPTOME -o ${OUTPUT}_auto-euk --auto-lineage-euk -m $ANALYSE -c $NCPUS --download_path $DB --offline"
else
        CMD="busco -i $TRANSCRIPTOME -o ${OUTPUT}_${LINEAGE} -l $LINEAGE -m $ANALYSE -c $NCPUS --download_path $DB --offline"
fi

echo $CMD > $LOGCMD
eval $CMD
